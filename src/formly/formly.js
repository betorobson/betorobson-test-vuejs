import Vue from 'vue'
import VueFormly from 'vue-formly'
import VueFormlyBootstrap from 'vue-formly-bootstrap'

import input from './input.vue'
import select from './select.vue'
import textarea from './textarea.vue'

Vue.use(VueFormly)
Vue.use(VueFormlyBootstrap)

VueFormly.addType('input', input)
VueFormly.addType('select', select)
VueFormly.addType('textarea', textarea)
