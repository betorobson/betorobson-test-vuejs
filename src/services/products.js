import axios from 'axios'

export default {
	post (attrs) {
		axios.post(
			'https://jsonplaceholder.typicode.com/posts',
			attrs.data
		)
		.then(response => {
			attrs.success(response)
		})
		.catch(e => {
			attrs.error()
		})
	}
}
