import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home'
import AddProduct from '@/views/AddProduct'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Home
		},
		{
			path: '/add-product',
			name: 'AddProduct',
			component: AddProduct
		}
	]
})
