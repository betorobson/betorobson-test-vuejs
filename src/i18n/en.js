export default {
	'en': {
		home: {
			title: 'betorobson vue test'
		},
		addProduct: {
			title: 'Add Product',
			iptIdProduct: 'Id',
			iptName: 'Name',
			iptCategory: 'Category',
			iptDescription: 'Description',
			btnSubmit: 'Submit'
		}
	}
}
