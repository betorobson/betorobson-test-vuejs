import Vue from 'vue'
import VueI18n from 'vue-i18n'

import en from './en'
import ptBR from './pt-br'

Vue.use(VueI18n)

let messages = {}

Object.assign(
	messages,
	en,
	ptBR
)

const i18n = new VueI18n({
	locale: 'en', // set locale
	fallbackLocale: 'en',
	messages // set locale messages
})

export default i18n
