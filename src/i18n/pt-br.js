export default {
	'pt-br': {
		home: {
			title: 'teste betorobson vue'
		},
		addProduct: {
			title: 'Adicionar produto',
			iptIdProduct: 'Código',
			iptName: 'Nome',
			iptCategory: 'Categoria',
			iptDescription: 'Descrição',
			btnSubmit: 'Enviar'
		}
	}
}
